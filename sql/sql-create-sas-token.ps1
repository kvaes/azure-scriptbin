$storageAccountName = "sqlhapremium"
$containerName = "sqldatafiles"
$resourceGroupName = "kvaes-test-sqlha"
$policyName = "kvaestestsqlhasas"

# Based on ; https://msdn.microsoft.com/en-us/library/dn466430.aspx

# Get the access keys for the ARM storage account
$accountKeys = Get-AzureRmStorageAccountKey -ResourceGroupName $resourceGroupName -Name $storageAccountName
# Create a new storage account context using an ARM storage account
$storageContext = New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $accountKeys.Key1
# Get the container in blob storage
$container = Get-AzureStorageContainer -Context $storageContext -Name $containerName
$cbc = $container.CloudBlobContainer

# Sets up a Stored Access Policy and a Shared Access Signature for the new container
$permissions = $cbc.GetPermissions();
$policyName = $policyName
$policy = new-object 'Microsoft.WindowsAzure.Storage.Blob.SharedAccessBlobPolicy'
## Start 5 minutes ago
$policy.SharedAccessStartTime = $(Get-Date).ToUniversalTime().AddMinutes(-5)
## End 10 years from now
$policy.SharedAccessExpiryTime = $(Get-Date).ToUniversalTime().AddYears(10)
## Give full permissions
$policy.Permissions = "Read,Write,List,Delete"
$permissions.SharedAccessPolicies.Add($policyName, $policy)
$cbc.SetPermissions($permissions);

# Gets the Shared Access Signature for the policy
$policy = new-object 'Microsoft.WindowsAzure.Storage.Blob.SharedAccessBlobPolicy'
$sas = $cbc.GetSharedAccessSignature($policy, $policyName)
Write-Host ''
Write-Host 'Shared Access Signature= '$($sas.Substring(1))''

# Outputs the Transact SQL to the clipboard and to the screen to create the credential using the Shared Access Signature
Write-Host ''
Write-Host '* Credential T-SQL'
Write-Host ''
$tSql = "CREATE CREDENTIAL [{0}] WITH IDENTITY='Shared Access Signature', SECRET='{1}'" -f $cbc.Uri,$sas.Substring(1) 
$tSql | clip
Write-Host $tSql