#!/bin/bash

STORAGEACCOUNT_NAME=$1
STORAGEACCOUNT_KEY=$2
if [ -z "$1" ]; then echo "Storage Account Name not set..." && exit 2 ; fi
if [ -z "$2" ]; then echo "Storage Account Key not set..." && exit 2 ; fi

echo "* Install Go / Cifs"
sudo apt-get install golang cifs-utils -y

echo "* Temp Structure"
TMP=/tmp/azurefile
mkdir -p $TMP
cd $TMP
rm -rf $TMP/src

echo "* Get files"
cd $TMP
git clone https://github.com/Azure/azurefile-dockervolumedriver src/azurefile
export GOPATH=`pwd`
cd src/azurefile
go get github.com/tools/godep
$TMP/bin/godep restore

echo "* Build"
go build

echo "* Copy binary"
AZUREVD="/usr/bin/azurefile-dockervolumedriver"
sudo cp $TMP/src/azurefile/azurefile $AZUREVD
sudo chmod +x $AZUREVD

echo "* Setup Service"
cd $TMP/src/azurefile/contrib/init/systemd
sudo cp azurefile-dockervolumedriver.default /etc/default/azurefile-dockervolumedriver
sudo cp azurefile-dockervolumedriver.service /etc/systemd/system/azurefile-dockervolumedriver.service

echo "* Setting Storage Account Info"
sudo sed -i "s|youraccount|$1|g" /etc/default/azurefile-dockervolumedriver
sudo sed -i "s|yourkey|$2|g" /etc/default/azurefile-dockervolumedriver

echo "* Starting Services"
sudo systemctl daemon-reload
sudo systemctl enable azurefile-dockervolumedriver
sudo systemctl start azurefile-dockervolumedriver