﻿$rgName = “kvaes-poc-firewall”
$vmName = (Get-AzureRmVM -ResourceGroupName $rgName).Name | Out-GridView -Title “Select a VM to configure forwarding …” -PassThru
$nicName = ((Get-AzureRmVM -ResourceGroupName $rgName -Name $vmName).NetworkInterfaceIDs).Split(“/”)[-1] | Out-GridView -Title “Select a NIC to configure forwarding …” -PassThru
$nicConfig = Get-AzureRmNetworkInterface -ResourceGroupName $rgName -Name $nicName
$nicConfig.EnableIPForwarding = $true
$nicConfig | Set-AzureRmNetworkInterface