﻿$routeTable01 = New-AzureRmRouteTable -Name "RT-Subnet001" -ResourceGroupName "kvaes-test-network" -Location "West Europe"
$routeTable01 = New-AzureRmRouteTable -Name "RT-Subnet001" -ResourceGroupName "kvaes-test-network" -Location "West Europe"
$vnet = Get-AzureRmVirtualNetwork -ResourceGroupName "kvaes-test-network" -Name "kvaes-test-network"
Set-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name "Subnet001" -AddressPrefix "192.168.1.0/24" -RouteTableId $routeTable01.Id | Set-AzureRmVirtualNetwork
Set-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name "Subnet002" -AddressPrefix "192.168.2.0/24" -RouteTableId $routeTable02.Id | Set-AzureRmVirtualNetwork
$routeTable01 | Add-AzureRmRouteConfig -Name "ToSubnet002" -AddressPrefix "192.168.2.0/24" -NextHopType VirtualAppliance -NextHopIpAddress "192.168.0.6" | Set-AzureRmRouteTable
$routeTable02 | Add-AzureRmRouteConfig -Name "ToSubnet001" -AddressPrefix "192.168.1.0/24" -NextHopType VirtualAppliance -NextHopIpAddress "192.168.0.6" | Set-AzureRmRouteTable
$routeTable01 | Add-AzureRmRouteConfig -Name "ToTheBatmobile" -AddressPrefix "0.0.0.0/0" -NextHopType VirtualAppliance -NextHopIpAddress "192.168.0.6" | Set-AzureRmRouteTable
$routeTable02 | Add-AzureRmRouteConfig -Name "ToTheBatmobile" -AddressPrefix "0.0.0.0/0" -NextHopType VirtualAppliance -NextHopIpAddress "192.168.0.6" | Set-AzureRmRouteTable