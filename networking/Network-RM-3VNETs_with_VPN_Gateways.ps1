﻿$subscriptionName = "XCS"
$resourceGroupName = "kvaes-test-vpn"
$locationName = "West Europe"

Login-AzureRmAccount
Select-AzureRmSubscription -SubscriptionName $subscriptionName
New-AzureRmResourceGroup -Name $resourceGroupName -Location $locationName

# First Network
$vnetName01 = "vnet01"
$vnetSubnetGateway01 = "10.1.0.0/28"
$vnetSubnetOne01 = "10.1.1.0/28"
$vnetAddressSpace01 = "10.1.0.0/16"
$subnet1 = New-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -AddressPrefix $vnetSubnetGateway01
$subnet2 = New-AzureRmVirtualNetworkSubnetConfig -Name 'Subnet1' -AddressPrefix $vnetSubnetOne01
New-AzureRmVirtualNetwork -Name $vnetName01 -ResourceGroupName $resourceGroupName -Location $locationName -AddressPrefix $vnetAddressSpace01 -Subnet $subnet1, $subnet2

# Second Network
$vnetName02 = "vnet02"
$vnetSubnetGateway02 = "10.2.0.0/28"
$vnetSubnetOne02 = "10.2.1.0/28"
$vnetAddressSpace02 = "10.2.0.0/16"
$subnet1 = New-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -AddressPrefix $vnetSubnetGateway02
$subnet2 = New-AzureRmVirtualNetworkSubnetConfig -Name 'Subnet1' -AddressPrefix $vnetSubnetOne02
New-AzureRmVirtualNetwork -Name $vnetName02 -ResourceGroupName $resourceGroupName -Location $locationName -AddressPrefix $vnetAddressSpace02 -Subnet $subnet1, $subnet2

# Third Network
$vnetName03 = "vnet03"
$vnetSubnetGateway03 = "10.3.0.0/28"
$vnetSubnetOne03 = "10.3.1.0/28"
$vnetAddressSpace03 = "10.3.0.0/16"
$subnet1 = New-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -AddressPrefix $vnetSubnetGateway03
$subnet2 = New-AzureRmVirtualNetworkSubnetConfig -Name 'Subnet1' -AddressPrefix $vnetSubnetOne03
New-AzureRmVirtualNetwork -Name $vnetName03 -ResourceGroupName $resourceGroupName -Location $locationName -AddressPrefix $vnetAddressSpace03 -Subnet $subnet1, $subnet2

# First Gateways
$lwgName01 = "lgw-vnet01"
$vnetGatewayName01 = "VNETGW01"
$vnetGatewayIP01 = New-AzureRmPublicIpAddress -Name $vnetGatewayName01 -ResourceGroupName $resourceGroupName -Location $locationName -AllocationMethod Dynamic
$vnet = Get-AzureRmVirtualNetwork -Name $vnetName01 -ResourceGroupName $resourceGroupName
$subnet = Get-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -VirtualNetwork $vnet
$gwipconfig = New-AzureRmVirtualNetworkGatewayIpConfig -Name gwipconfig1 -SubnetId $subnet.Id -PublicIpAddressId $vnetGatewayIP01.Id
New-AzureRmVirtualNetworkGateway -Name $vnetGatewayName01 -ResourceGroupName $resourceGroupName -Location $locationName -IpConfigurations $gwipconfig -GatewayType Vpn -VpnType RouteBased -GatewaySku Basic
$vnetGatewayIP01 = Get-AzureRmPublicIpAddress -Name $vnetGatewayName01 -ResourceGroupName $resourceGroupName
New-AzureRmLocalNetworkGateway -Name $lwgName01 -ResourceGroupName $resourceGroupName -Location $locationName -GatewayIpAddress $vnetGatewayIP01.IpAddress -AddressPrefix $vnetAddressSpace01

# Second Gateways
$lwgName02 = "lgw-vnet02"
$vnetGatewayName02 = "VNETGW02"
$vnetGatewayIP02 = New-AzureRmPublicIpAddress -Name $vnetGatewayName02 -ResourceGroupName $resourceGroupName -Location $locationName -AllocationMethod Dynamic
$vnet = Get-AzureRmVirtualNetwork -Name $vnetName02 -ResourceGroupName $resourceGroupName
$subnet = Get-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -VirtualNetwork $vnet
$gwipconfig = New-AzureRmVirtualNetworkGatewayIpConfig -Name gwipconfig2 -SubnetId $subnet.Id -PublicIpAddressId $vnetGatewayIP02.Id
New-AzureRmVirtualNetworkGateway -Name $vnetGatewayName02 -ResourceGroupName $resourceGroupName -Location $locationName -IpConfigurations $gwipconfig -GatewayType Vpn -VpnType RouteBased -GatewaySku Basic
$vnetGatewayIP02 = Get-AzureRmPublicIpAddress -Name $vnetGatewayName02 -ResourceGroupName $resourceGroupName
New-AzureRmLocalNetworkGateway -Name $lwgName02 -ResourceGroupName $resourceGroupName -Location $locationName -GatewayIpAddress $vnetGatewayIP02.IpAddress -AddressPrefix $vnetAddressSpace02

# Third Gateways
$lwgName03 = "lgw-vnet03"
$vnetGatewayName03 = "VNETGW03"
$vnetGatewayIP03 = New-AzureRmPublicIpAddress -Name $vnetGatewayName03 -ResourceGroupName $resourceGroupName -Location $locationName -AllocationMethod Dynamic
$vnet = Get-AzureRmVirtualNetwork -Name $vnetName03 -ResourceGroupName $resourceGroupName
$subnet = Get-AzureRmVirtualNetworkSubnetConfig -Name 'GatewaySubnet' -VirtualNetwork $vnet
$gwipconfig = New-AzureRmVirtualNetworkGatewayIpConfig -Name gwipconfig3 -SubnetId $subnet.Id -PublicIpAddressId $vnetGatewayIP03.Id
New-AzureRmVirtualNetworkGateway -Name $vnetGatewayName03 -ResourceGroupName $resourceGroupName -Location $locationName -IpConfigurations $gwipconfig -GatewayType Vpn -VpnType RouteBased -GatewaySku Basic
$vnetGatewayIP03 = Get-AzureRmPublicIpAddress -Name $vnetGatewayName03 -ResourceGroupName $resourceGroupName
New-AzureRmLocalNetworkGateway -Name $lwgName03 -ResourceGroupName $resourceGroupName -Location $locationName -GatewayIpAddress $vnetGatewayIP03.IpAddress -AddressPrefix $vnetAddressSpace03

# Create Connections
$vpn01 = Get-AzureRmVirtualNetworkGateway -Name $vnetGatewayName01 -ResourceGroupName $resourceGroupName
$vpn02 = Get-AzureRmVirtualNetworkGateway -Name $vnetGatewayName02 -ResourceGroupName $resourceGroupName
$vpn03 = Get-AzureRmVirtualNetworkGateway -Name $vnetGatewayName03 -ResourceGroupName $resourceGroupName
$lwg01 = Get-AzureRmLocalNetworkGateway -Name $lwgName01 -ResourceGroupName $resourceGroupName
$lwg02 = Get-AzureRmLocalNetworkGateway -Name $lwgName02 -ResourceGroupName $resourceGroupName
$lwg03 = Get-AzureRmLocalNetworkGateway -Name $lwgName03 -ResourceGroupName $resourceGroupName
$key12 = "key12"
$name12 = "conn12"
$name21 = "conn21"
New-AzureRmVirtualNetworkGatewayConnection -Name $name12 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn01 -LocalNetworkGateway2 $lwg02 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key12
New-AzureRmVirtualNetworkGatewayConnection -Name $name21 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn02 -LocalNetworkGateway2 $lwg01 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key12
$key23 = "key23"
$name23 = "conn23"
$name32 = "conn32"
New-AzureRmVirtualNetworkGatewayConnection -Name $name23 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn02 -LocalNetworkGateway2 $lwg03 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key23
New-AzureRmVirtualNetworkGatewayConnection -Name $name32 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn03 -LocalNetworkGateway2 $lwg02 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key23
$key31 = "key31"
$name31 = "conn31"
$name13 = "conn13"
New-AzureRmVirtualNetworkGatewayConnection -Name $name31 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn03 -LocalNetworkGateway2 $lwg01 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key31
New-AzureRmVirtualNetworkGatewayConnection -Name $name13 -ResourceGroupName $resourceGroupName -Location $locationName -VirtualNetworkGateway1 $vpn01 -LocalNetworkGateway2 $lwg03 -ConnectionType IPsec -RoutingWeight 10 -SharedKey $key31