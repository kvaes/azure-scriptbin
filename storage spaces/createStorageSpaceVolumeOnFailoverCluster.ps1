$storagePoolName = "XylosStoragePoolLocal"
$virtualDiskName = "XylosVirtualDiskLocal"
$driveLetter = "Z"
Write-Host "* Looking for the storage sub system with Automatic Clustering enabled"
$storageSubSystem = Get-StorageSubSystem | Where AutomaticClusteringEnabled -eq $true
if ($storageSubSystem -eq $null) {
    Write-Host "* No storage sub system found with Automatic Clustering enabled... Please choose one ; "
    $storageSubSystem = Get-StorageSubSystem | Out-GridView -PassThru
} else {
    Write-Host "* Disabling the Automatic Clustering"
    Get-StorageSubSystem | Where AutomaticClusteringEnabled -eq $true | Set-StorageSubSystem -AutomaticClusteringEnabled $false
}
Write-Host "* Using" $storageSubSystem.FriendlyName "as storage sub system"
$columnCount = (Get-PhysicalDisk -CanPool $true).Count
Write-Host "* Using a column count of" $columnCount
Write-Host "* Creating storage pool" $storagePoolName
$storagePool = New-StoragePool -FriendlyName $storagePoolName -StorageSubSystemUniqueId $storageSubSystem.uniqueID -PhysicalDisks (Get-PhysicalDisk -StorageSubSystem $storageSubSYstem -CanPool $true)
Write-Host "* Creating virtual disk name" $virtualDiskName 
$virtualDisk = New-VirtualDisk -StoragePoolFriendlyName $storagePoolName -FriendlyName $virtualDiskName -ResiliencySettingName "Simple" -NumberOfColumns $columnCount -UseMaximumSize
Write-Host "* Initializing the virtual disk as drive" $driveLetter
Get-VirtualDisk –FriendlyName $virtualDiskName | Get-Disk | Initialize-Disk –Passthru | New-Partition -DriveLetter $driveLetter –UseMaximumSize | Format-Volume -Confirm:$false